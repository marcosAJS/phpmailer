<?php
require './phpmailer/class.phpmailer.php';
class Email extends PHPMailer {

    private $email;
    private $body;
    private $quem;
    private $para;
    private $copia;
    private $assunto;
    private $mensagem;

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getBody() {
        return $this->body;
    }

    public function setBody($body) {
        $this->body = $body;
    }

    public function getQuem() {
        return $this->quem;
    }

    public function setQuem($quem) {
        $this->quem = $quem;
    }

    public function getPara() {
        return $this->para;
    }

    public function setPara($para) {
        $this->para = $para;
    }

    public function getCopia() {
        return $this->copia;
    }

    public function setCopia($copia) {
        $this->copia = $copia;
    }

    public function getAssunto() {
        return $this->assunto;
    }

    public function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    public function getMensagem() {
        return $this->mensagem;
    }

    public function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    public function enviarEmail() {
        $this->CharSet = "UTF-8";
        $this->SMTPSecure = "ssl";
        $this->isSMTP();
        $this->Host = "smtp.gmail.com";
        $this->Port = "465";
        $this->SMTPAuth = true;
        $this->Username = "email";
        $this->Password = "senha";
        $this->isHTML();

        //De quem é o email de envio
        $this->setFrom("email que vai mandar");
        $this->FromName = $this->quem;
        $this->addAddress($this->para);
        $copia = $this->copia;

        if (!empty($copia)) {
            $this->addAddress($copia);
        }

        $this->Subject = $this->assunto;
        $this->AltBody = "Para você visualizar este email, tenha certeza de estar em um programa que aceite interprete html";
        $this->msgHTML($this->body);

        if (!$this->send()) {
            echo $this->ErrorInfo;
            return false;
        } else {

            return true;
        }
    }

}
